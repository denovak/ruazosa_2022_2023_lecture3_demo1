package hr.fer.ruazosa.lecture3demo1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import hr.fer.ruazosa.lecture3demo1.databinding.ActivityEditFirstAndLastNameBinding
import hr.fer.ruazosa.lecture3demo1.databinding.ActivityMainBinding

class EditFirstAndLastNameActivity : AppCompatActivity() {

    private lateinit var binding: ActivityEditFirstAndLastNameBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEditFirstAndLastNameBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.lastNameEditTextViewId.setText(FirstAndLastName.lastName)
        binding.firstNameTextViewId.setText(FirstAndLastName.firstName)

        binding.saveFirstAndLastNameButtonId.setOnClickListener {
            FirstAndLastName.firstName = binding.firstNameTextViewId.text.toString()
            FirstAndLastName.lastName = binding.lastNameEditTextViewId.text.toString()
            finish()
        }

    }
}