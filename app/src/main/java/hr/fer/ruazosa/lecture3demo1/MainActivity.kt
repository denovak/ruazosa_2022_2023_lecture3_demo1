package hr.fer.ruazosa.lecture3demo1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import hr.fer.ruazosa.lecture3demo1.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        setFirstAndLastName()

        binding.editFirstAndLastNameButtonId.setOnClickListener {
            val editFirstAndLastNameIntent = Intent(this, EditFirstAndLastNameActivity::class.java)
            startActivity(editFirstAndLastNameIntent)
        }
    }

    override fun onResume() {
        super.onResume()
        setFirstAndLastName()
    }

    private fun setFirstAndLastName() {
        binding.firstNameTextiViewId.text = "Your first name is: " + FirstAndLastName.firstName
        binding.lastNameTextViewId.text = "Your last name is: " + FirstAndLastName.lastName
    }
}